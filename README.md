# Documentation

## Homepage
https://velox.swiss

## Source code
https://gitlab.com/velox-shop

## Project documentation

To get an overview about the architecture, requirements, conventions and so on, have a look at the wiki.

https://gitlab.com/velox-shop/documentation/-/wikis/home

## API documentation
https://app.swaggerhub.com/apis/velox-swiss


# Getting started - Please also read ''Known issues'' section

*If you want to contribute to the Velox feel free to create issues and merge requests on project in GitLab.* 

*https://gitlab.com/velox-shop/documentation/-/wikis/Collaboration*

## Running Velox locally
To run Velox locally you can directly start it using docker-compose see:  https://gitlab.com/velox-shop/velox-demo 


## Local development & running the service
To develop service locally clone desired service and make desired changes. In order to run and test the service locally follow the documentation provided in the service repository. In general, backend services can be run with gradle or docker, while the frontend with node or docker. Some services, e.g. orchestration-service, depend on other services and therefore their dependencies should be run with docker-compose. All necessary details can be found in the documentation of each service.

## Local development and running complete Velox solution
### To develop backend service locally and test it in Velox follow the next steps: 

1. Clone velox-demo project, desired service and make desired changes in the service
2. Build the service
    
    1. Using gradle: `./gradlew clean build`
    2. Or using the docker: `docker build -t service-image-name .`  
3. If in step 2. you used gradle, remove/comment the lines that specify the docker-container related to changed service in docker-compose file in the velox-demo project (now local instance is used). Furthermore, adapt environment variable url in docker-containers that depend on the changed service to point to the url at which the local service will be run. Run the service with gradle: `./gradlew bootRun`    
If in step 2. you used docker, then just change the name of the image in the docker-compose file that is used for creating the changed service container, example:  
```
container-name: 
    ... 
    image: service-image-name
    ...
```
4. Build docker-compose from the velox-demo (it runs all other services)

At this point you should be running the Velox locally with your local service changes. 

    
### To develop frontend service locally and test it in Velox follow the next steps:
1. Clone velox-demo project, desired frontend project (ui/backoffice-ui) and make desired changes
2. Run `npm install` to download node modules
3. Run the frontend with `npm start` or with docker (explained in the frontend project documentation)
4. Remove/comment the lines that specify the docker-container related to changed frontend service in docker-compose file in the velox-demo project (now local instance is used)
5. Build docker-compose from the velox-demo (it runs all other services)

## ZITADEL accounts 
Velox uses [ZITADEL](https://zitadel.ch/) as a identity and access management solution.

When running velox-demo, in order to create orders as a customer you will have to create ZITADEL account. You will be prompted to create one when you try to login to Velox using Velox frontend. 


If you want to access the backoffice started with velox-demo, you need a ZITADEL user with admin permission. You can use the following e-mail and password to login with an appropriate user:
- E-mail: admin-local@velox.shop
- Password: Password_1

To be able to import the initial data to you local VELOX instance, you we need the:
- client secret: dcVqdj1cWAudyL1ddRcmnPb0ssT1SxiulzeNQgVX8h0a3zMnHFO1omwRcIlhjra
- oauth token: nvTemxjlOqmziVMdKOEYmUICAOpzRUp-tfab_RRVpqh-uB0eOmsohsEaT8gvHGJP3YyFVIg (this is the token of a service user)

# Known issues
## Environment variables in velox-demo docker-compose
Currently, velox-demo project only supports one type of running environment. In our case, the environment variables in docker-compose file are set for staging purpose.
This currently presents an obstacle, since, for example, in orded to run FE services (ui/backoffice-ui) locally with docker-compose file from velox-demo project, section with specification of their environment variables has to be removed from docker-compose (so the default values for the local can be used).

Some potential solutions for this problem are:
1. Usage of different configuration files
2. Usage of GitLab CI/CD environment variables
3. Different docker-compose files 

## Zitadel accounts
Zitadel configuration in our projects is static. This doesn't allow creation of test applications in Zitadel for interested parties and the dynamic use of their parameters, like client-id that is application specific, since configuration has to be overwritten in each project manually.
